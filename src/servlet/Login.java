package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.ResultBean;
import entity.UserBean;
import util.SHA;

@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String SUCCESS = "/WEB-INF/index.jsp";
	private static final String FALT = "login.jsp";
	private static final String PHONE_FALT = "phone-login.jsp";
       
    public Login() {
        super();
        
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String phone = request.getParameter("phone");
		String url = "";
		UserBean user = new UserBean(username,password);
		ResultBean<?> result = user.login();
		if(result.getCode() == ResultBean.PASSWORD_ERROR || result.getCode() == ResultBean.USER_NOT_EXIST){
			if(phone.equals("true")){
				url = PHONE_FALT;
			}else{
				url = FALT;
			}
		}else{
			url = SUCCESS;
			request.setAttribute("username", username);
			try {
				request.setAttribute("password", SHA.encryptSHA(password));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		request.setAttribute("result", result);
		
		RequestDispatcher dis = request.getRequestDispatcher(url);
		dis.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
