package jdbcutil;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;

public class External_JDBC {
	
	private static String driver = null;
	private static String url = null;
	private static String username = null;
	private static String password = null;
	
	static{
		External_Config config = new External_Config();
		driver = config.getDriver();
		url = config.getUrl();
		username = config.getUsername();
		password = config.getPassword();
		try{
			Class.forName(driver);
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection(){
		Connection conn = null;
		try{
			conn = (Connection) DriverManager.getConnection(url, username, password);
		}catch(SQLException e){
			e.printStackTrace();
		}
		return conn;
	}
	
	public static void freeConnection(Connection conn){
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void freeStatement(Statement stat){
		try {
			stat.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void freeResultSet(ResultSet rs){
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void free(Connection conn, Statement stat, ResultSet rs){
		if (rs != null) {  
            freeResultSet(rs);  
        }  
        if (stat != null) {  
            freeStatement(stat);  
        }  
        if (conn != null) {  
            freeConnection(conn);  
        }  
	}
}
