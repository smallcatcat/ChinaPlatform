package jdbcutil;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mysql.jdbc.ResultSetMetaData;

/**
 * 数据库访问帮助类
 * @author microsoft
 *
 */
public class JdbcHelper {

	private static Connection conn = null;
	private static PreparedStatement preparedStatement = null;
	private static CallableStatement callableStatement = null;
	
	/**
	 * 用于查询，返回结果集
	 * 
	 * @param sql
	 * @return
	 */
	public static List query(String sql){
		ResultSet rs = null;
		try{
			getPreparedStatement(sql);
			rs = preparedStatement.executeQuery();
			return ResultToListMap(rs);
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			free(rs);
		}
		return null;
	}
	
	
	/**
	 * 用于带参数的查询，返回结果集
	 * 
	 * @param sql
	 * @param paramters
	 * @return
	 */
	public static List query(String sql, Object... paramters){
		ResultSet rs = null;
		try{
			getPreparedStatement(sql);
			
			for(int i = 0 ; i < paramters.length ; i++){
				preparedStatement.setObject(i+1, paramters[i]);
			}
			
			rs = preparedStatement.executeQuery();
			return ResultToListMap(rs);
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			free(rs);
		}
		return null;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" }) 
	private static List ResultToListMap(ResultSet rs){
		List list = new ArrayList();
		try{
			while(rs.next()){
				ResultSetMetaData md = (ResultSetMetaData) rs.getMetaData();
				Map map = new HashMap();
				for(int i = 1 ; i <= md.getColumnCount(); i++){
					map.put(md.getColumnLabel(i), rs.getObject(i));
				}
				list.add(map);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return list;
	}


	/**
	 * 获取PreparedStatement
	 * 
	 * @param sql 
	 * @throws SQLException
	 */
	public static void getPreparedStatement(String sql) throws SQLException{
		conn = JDBCUtil.getConnection();
		preparedStatement = conn.prepareStatement(sql);
	}
	
	
	/**
	 * 获取CallableStatement
	 * 
	 * @param procedureSql
	 * @throws SQLException
	 */
	public static void getCallableStatement(String procedureSql) throws SQLException{
		conn = JDBCUtil.getConnection();
		callableStatement = conn.prepareCall(procedureSql);
	}
	
	/**
	 * 释放资源
	 * 
	 * @param rs 结果集
	 */
	public static void free(ResultSet rs){
		JDBCUtil.free(conn, preparedStatement, rs);
	}
	
	/**
	 * 释放资源
	 * 
	 * @param statement
	 * @param rs
	 */
	public static void free(Statement statement, ResultSet rs){
		JDBCUtil.free(conn, statement, rs);
	}
	
	public static void main(String [] args){
		List list = query("select * from cp_user where id = ?",9);
		
		System.out.println(((Map)list.get(0)).get("cp_username"));
	}
}
