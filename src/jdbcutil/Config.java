package jdbcutil;

import java.io.FileInputStream;
import java.util.Properties;

import java.io.InputStream;

public class Config {
	private String driver = null;
	private String url = null;
	private String username = null;
	private String password = null;
	public Config(){
		Properties pro = new Properties();
		try{
			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("jdbc.properties");  
			pro.load(inputStream);
			inputStream.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		this.driver = pro.getProperty("driver");
		this.url = pro.getProperty("url");
		this.username = pro.getProperty("username");
		this.password = pro.getProperty("password");
	}
	
	public String getDriver(){
		return driver;
	}
	
	public String getUrl(){
		return url;
	}
	
	public String getUsername(){
		return username;
	}
	
	public String getPassword(){
		return password;
	}
}
