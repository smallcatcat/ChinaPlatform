package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Connection;
import java.sql.Statement;

import jdbcutil.External_JDBC;
import jdbcutil.JDBCUtil;
import util.SHA;

public class UserBean {
	private String username = "";
	private String password = "";
	
	public UserBean(){
		
	}
	public UserBean(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * 存储用户
	 * @return ResultBean
	 */
	public ResultBean<?> save(){
		ResultBean<?> rb = new ResultBean<String>(username);
		Connection conn = JDBCUtil.getConnection();
		Connection external = External_JDBC.getConnection();
		PreparedStatement prestat = null;
		PreparedStatement external_prestat = null;
		try {
			if(conn != null && username != null && password != null){
				String selectAll = "select * from cp_user where cp_username = ?";
				prestat = conn.prepareStatement(selectAll);
				prestat.setString(1, username);
				ResultSet rs = prestat.executeQuery();
				if(!rs.next()){
					String sql = "insert into cp_user(id,cp_username,cp_password) values(?,?,?)";
					prestat = conn.prepareStatement(sql);
					prestat.setString(1, null);
					prestat.setString(2, username);
					//同时注册考试系统
					String sql1 = "insert into user(USERNO,PASSWORD,ISEXAM) values(?,?,?)";
					external_prestat = external.prepareStatement(sql1);
					external_prestat.setString(1, username);
					external_prestat.setInt(3, 0);
					try{
						prestat.setString(3, SHA.encryptSHA(password));
						external_prestat.setString(2, SHA.encryptSHA(password));
					}catch(Exception e){
						e.printStackTrace();
					}
					int result = prestat.executeUpdate();
					int result1 = external_prestat.executeUpdate();
					if(result != 0 && result1 != 0){
						rb.setMsg("注册成功");
						rb.setCode(ResultBean.REG_SUCCESS);
					}else{
						rb.setMsg("注册失败");
						rb.setCode(ResultBean.REG_FALT);
					}
				}else{
					rb.setMsg("用户名已存在");
					rb.setCode(ResultBean.REG_FALT);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try{
				if(prestat != null){
					prestat.close();
				}
				if(external_prestat != null){
					external_prestat.close();
				}
				if(conn != null){
					conn.close();
				}
				if(external != null){
					external.close();
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		return rb;
	}
	
	/**
	 * 检查用户名是否合法
	 * @param username 用户名
	 * @return ResultBean
	 */
	public ResultBean checkUsername(String username){
		ResultBean rb = new ResultBean<>();
		if(username.isEmpty()){
			rb.setMsg("用户名不能为空！");
			rb.setCode(ResultBean.USERNAME_EMPTY);
		}
		for(int i = 0 ; i < username.length() ; i++){
			char ch = username.charAt(i);
			if( (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9') || ch == '_' || ch == '@' || username.length() > 16){
				continue;
			}else{
				rb.setMsg("用户名不合法！");
				rb.setCode(ResultBean.USERNAME_ILLEGAL);
				break;
			}
		}
		return rb;
	}
	
	/**
	 * 用户登录
	 * @return ResultBean
	 */
	public ResultBean<?> login(){
		ResultBean<?> rb = new ResultBean<String>(username);
		Connection conn = JDBCUtil.getConnection();
		PreparedStatement prestat = null;
		try{
			String sql = "select cp_username,cp_password from cp_user where cp_username = ?";
			prestat = conn.prepareStatement(sql);
			prestat.setString(1, username);
			ResultSet rs = prestat.executeQuery();
			if(!rs.next()){
				rb.setCode(ResultBean.USER_NOT_EXIST);
				rb.setMsg("用户名不存在！");
			}else{
				String encodePassword = null;
				try{
					encodePassword = SHA.encryptSHA(password);
				}catch(Exception e){
					e.printStackTrace();
				}
				if(encodePassword.equals(rs.getString("cp_password"))){
					rb.setCode(ResultBean.SUCCESS);
					rb.setMsg("登录成功！");
				}else{
					rb.setCode(ResultBean.PASSWORD_ERROR);
					rb.setMsg("密码错误！");
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				if(prestat != null){
					prestat.close();
				}
				if(conn != null){
					conn.close();
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		return rb;
	}
}
