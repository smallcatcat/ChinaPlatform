package entity;

import java.io.Serializable;

public class ResultBean<T> implements Serializable{
	public static final int SUCCESS = 0;
	public static final int FALT = 1;
	public static final int USER_EXIST = 2;
	public static final int USERNAME_EMPTY = 3;
	public static final int USERNAME_ILLEGAL = 4;
	public static final int USER_NOT_EXIST = 5;
	public static final int PASSWORD_ERROR = 6;
	public static final int REG_FALT = 7;
	public static final int REG_SUCCESS = 8;
	
	private String msg = "success";
	
	private int code = SUCCESS;
	private T data;
	
	public ResultBean(){
		super();
	}
	
	public ResultBean(T data){
		super();
		this.data = data;
	}
	
	public ResultBean(Throwable e){
		super();
		this.msg = e.toString();
		this.code = FALT;
	}
	
	public void setCode(int code){
		this.code = code;
	}
	
	public int getCode(){
		return code;
	}
	
	public void setMsg(String msg){
		this.msg = msg;
	}
	public String getMsg(){
		return msg;
	}
	public T getData(){
		return data;
	}
}
