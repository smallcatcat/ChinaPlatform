<%@ page language="java" contentType="text/html; charset=utf-8"
	isELIgnored="false" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>用户注册</title>
<link rel="stylesheet" href="style/login.css" />
<script>
		function uaredirect(murl){
			try {
				if(document.getElementById("bdmark") != null){
				return;
				}
				var urlhash = window.location.hash;
				if (!urlhash.match("fromapp")){
					if ((navigator.userAgent.match(/(iPhone|iPod|Android|ios|iPad)/i))) {
						location.replace(murl);
					}
				}
				
			} catch(err){
			
			}
		}
	</script>
<script type="text/javascript">window.onload = uaredirect("phone-login.jsp")</script>
</head>
<body>
	<div class="box">
		<div class="main">
			<div class="main_right">
				<div class="main_right_body">
					<h4>
						注册<span>${result.msg }</span>
					</h4>
					<form onsubmit="return chack()" action="Register" method="POST">
						<input type="hidden" name="phone" value="false" />
						<p>
							<input type="text" id="usename" name="username"
								placeholder="请输入您的账号" />
						</p>
						<p>
							<input type="password" id="password" name="password"
								placeholder="密码" />
						</p>
						<p>
							<input type="password" id="confirm_password"
								name="confirm_password" placeholder="确认密码" />
						</p>
						<p>
							<input type="submit" value="注册" class="bt" /><a href="login.jsp">登录</a>
						</p>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script>
			function chack(){
				var un=document.getElementById("usename").value;
				var pw=document.getElementById("password").value;
				var cp=document.getElementById("confirm_password").value;
				if(!/^[a-zA-Z0-9_]+$/.test(un)){
					alert("账号不合法");
					return false;
				}
				if(!/^[a-zA-Z0-9]+$/.test(pw)){
					alert("密码不合法");
					return false;
				}
				else if(cp!=pw){
			    	alert("两次密码不一致");
			    	return false;}
				else 
				return true;
			}
		</script>
</body>
</html>