/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : chinaplatform

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-12-27 22:19:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cp_user`
-- ----------------------------
DROP TABLE IF EXISTS `cp_user`;
CREATE TABLE `cp_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cp_username` varchar(16) NOT NULL,
  `cp_password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cp_user
-- ----------------------------
INSERT INTO `cp_user` VALUES ('31', '123', '40bd001563085fc35165329ea1ff5c5ecbdbbeef');
INSERT INTO `cp_user` VALUES ('32', '1234', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220');
