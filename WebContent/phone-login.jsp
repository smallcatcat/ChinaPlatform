<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
	name="viewport">
<title>login</title>
<link rel="stylesheet" href="style/telephone.css" />
</head>

<body
	style="background-image: url(img/gif/cat.gif); background-repeat: no-repeat; background-size: cover;">

	<div class="box">
		<div class="main">
			<div>
				<div class="main-body" style="border: none;">
					<div class="title"><h1>用户登录<span>${result.msg }</span></h1></div>
					<form class="form-horizontal" action="Login" method="POST">
						<input type="hidden" name="phone" value="true" />
						<div class="form-group">
							<div class="form p input">
								<input type="text" class="form-control" id="inputEmail3"
									name="username" value="QQ/微信号/手机号">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-10">
								<input type="text" class="form-control" id="inputPassword3"
									name="password" value="请输入你的密码">
							</div>
						</div>
						<div class="form-group button">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-default">登陆</button>
							</div>
						</div>
						<div class="form-group text">
							<div class="col-sm-offset-2 col-sm-10">
								<a href="phone-register.jsp" class="register">注册</a> <a herf=""
									class="forget">忘记密码？</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script>
				function chack() {
					var usename = document.getElementById("inputEmail3");
					usename.onfocus = function(){
						if(usename.value == 'QQ/微信号/手机号'){
							usename.value = '';
						}
						
					}
		
					usename.onblur = function(){
						if(usename.value == ''){
							usename.value = 'QQ/微信号/手机号';
						}
					}
					var password = document.getElementById("inputPassword3");
					password.onblur = function(){
						if(password.value == ''){
							password.value = '请输入你的密码';
							password.type = "text"
						}
					}
					password.onfocus = function(){
						if(password.value == '请输入你的密码'){
							password.value = '';
							password.type = "password"
						}
					}
					
				}
				chack();
			</script>
</body>

</html>