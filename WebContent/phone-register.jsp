<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
	name="viewport">
<title>register</title>
<link rel="stylesheet" href="style/telephone.css" />
</head>

<body
	style="background-image: url(img/gif/cat.gif); background-repeat: no-repeat; background-size: cover;">

	<div class="box">
		<!--<div class="top">
				<img src="../../img/porcelain-carving.jpg">
			</div>-->
		<div class="main">
			<div>
				<div class="main-body" style="border: none;">
					<div class="title"><h1>用户注册<span>${result.msg }</span></h1></div>
					<form class="form-horizontal" onsubmit="return check_legal()" action="Register" method="POST">
						<input type="hidden" name="phone" value="true" />
						<div class="form-group">
							<div class="form p input">
								<input type="text" class="form-control" id="usename" name="username"
									value="用户名">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-10">
								<input type="text" class="form-control" id="password" name="password"
									value="请输入你的密码">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-10">
								<input type="text" class="form-control" id="confirm_password" name="confirm_password"
									value="确认密码">
							</div>
						</div>
						<div class="form-group button">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-default">注册</button>
							</div>
						</div>
						<div class="form-group text">
							<div class="col-sm-offset-2 col-sm-10">
								<a href="phone-login.jsp" class="register">登陆</a> <a herf=""
									class="forget">出现问题？</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script>
		function check_legal(){
			var un=document.getElementById("usename").value;
			var pw=document.getElementById("password").value;
			var cp=document.getElementById("confirm_password").value;
			if(!/^[a-zA-Z0-9_]+$/.test(un)){
				alert("账号不合法");
				return false;
			}
			if(!/^[a-zA-Z0-9]+$/.test(pw)){
				alert("密码不合法");
				return false;
			}
			else if(cp!=pw){
		    	alert("两次密码不一致");
		    	return false;}
			else 
			return true;
		}
		function chack() {
			var usename = document.getElementById("usename");
			usename.onfocus = function(){
				if(usename.value == '用户名'){
					usename.value = '';
				}
				
			}
			usename.onblur = function(){
				if(usename.value == ''){
					usename.value = '用户名';
				}
			}
			var password1 = document.getElementById("password");
			var password2 = document.getElementById("confirm_password");
			password1.onblur = function(){
				if(password1.value == ''){
					password1.value = '请输入你的密码';
					password1.type = "text"
				}
			}
			password1.onfocus = function(){
				if(password1.value == '请输入你的密码'){
					password1.value = '';
					password1.type = "password"
				}
			}
			password2.onblur = function(){
				if(password2.value == ''){
					password2.value = '确认密码';
					password2.type = "text"
				}
			}
			password2.onfocus = function(){
				if(password2.value == '确认密码'){
					password2.value = '';
					password2.type = "password"
				}
			}
		}
		chack();
	</script>
</body>

</html>